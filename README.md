# Bfunc

Efunc is an ocaml ethereum client.
The documentation of the library can be found [here](https://functori.gitlab.io/dev/efunc/efunc/Eth/index.html).

## Install

Bfunc can be installed throught [opam](https://opam.ocaml.org/doc/Install.html):
```bash
opam pin add bfunc.~dev https://gitlab.com/functori/dev/bfunc
```

## License
Copyright © 2021, Functori <contact@functori.com>. Released under the [MIT License](https://gitlab.com/functori/bfunc/-/blob/master/LICENSE).
