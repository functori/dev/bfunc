open Btc

let (let>) = Lwt.bind
let (let>?) p ok = Lwt.bind p (Result.fold ~ok ~error:Lwt.return_error)

let wrap_btc p = Fun.flip Lwt.map p (Result.map_error (fun e -> `btc e))

let get_hash = function
  | `level h -> post (Blockhash h)
  | `head -> post Best
  | `hash id -> Lwt.return_ok id

let block id =
  wrap_btc @@
  let>? id = get_hash id in
  post (Block2 id)

let sleep = ref 5.

let listen f acc =
  let rec aux acc =
    let> r = wrap_btc (post Best) in
    let> r = f acc r in
    match r with
    | `continue acc ->
      let> () = EzLwtSys.sleep !sleep in
      aux acc
    | `stop r -> Lwt.return r in
  aux acc

let hash b = b.header.hash
let predecessor b = b.header.previousblockhash
let level b = b.header.height

let util = { Crawl.hash; level ; predecessor }
let chain = { Crawl.block; listen = `separate listen }
