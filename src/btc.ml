include Types
include Utils.Let
module Services = Services
module Utils = Utils

let stopr = ref false
let stop _ = Lwt.return !stopr

let post = Utils.post
