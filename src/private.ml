type b = string
let b_enc : b Json_encoding.encoding =
  Json_encoding.(conv (fun b -> "0x" ^ b) (fun s -> String.sub s 2 (String.length s - 2)) string)
let b s = s
let b_empty : b = ""
