open Json_encoding

type 'arg input = {
  meth: string; [@key "method"]
  arg: 'arg;
  params: Json_repr.ezjsonm list;
  id: Json_repr.ezjsonm;
}

let input0_enc =
  conv
    (fun {meth; params; id; _} -> (), meth, params, id)
    (fun ((), meth, params, id) -> {meth; arg=(); params; id}) @@
  obj4
    (req "jsonrpc" (constant "1.0"))
    (req "method" string)
    (Json_encoding.dft "params" (list any_ezjson_value) [])
    (Json_encoding.dft "id" any_ezjson_value `Null)

let input1_enc enc =
  conv
    (fun {meth; arg; params; id} ->
       let params = Json_encoding.construct enc arg :: params in
       (), meth, params, id)
    (fun ((), meth, params, id) ->
       let arg, params = match params with
         | x :: params -> Json_encoding.destruct enc x, params
         | _ -> assert false in
       {meth; arg; params; id}) @@
  obj4
    (req "jsonrpc" (constant "1.0"))
    (req "method" string)
    (Json_encoding.dft "params" (list any_ezjson_value) [])
    (Json_encoding.dft "id" any_ezjson_value `Null)

let input2_enc enc1 enc2 =
  conv
    (fun {meth; arg=(arg1, arg2); params; id} ->
       let p1 = Json_encoding.construct enc1 arg1 in
       let p2 = Json_encoding.construct enc2 arg2 in
       let params = p1 :: p2 :: params in
       (), meth, params, id)
    (fun ((), meth, params, id) ->
       let arg, params = match params with
         | x1 :: x2 :: params ->
           let a1 = Json_encoding.destruct enc1 x1 in
           let a2 = Json_encoding.destruct enc2 x2 in
           (a1, a2), params
         | _ -> assert false in
       {meth; arg; params; id}) @@
  obj4
    (req "jsonrpc" (constant "1.0"))
    (req "method" string)
    (Json_encoding.dft "params" (list any_ezjson_value) [])
    (Json_encoding.dft "id" any_ezjson_value `Null)

type error = {
  code: int;
  message: string;
  data: string option;
} [@@deriving encoding]

type 'a output = ('a, error) result

let output_enc enc =
  union [
    case (EzEncoding.ignore_enc (obj1 (req "result" enc)))
      (function Ok x -> Some x | _ -> None)
      (fun x -> Ok x);
    case (EzEncoding.ignore_enc (obj1 (req "error" error_enc)))
      (function Error e -> Some e | _ -> None)
      (fun e -> Error e);
  ]

type bip9 = {
  status: [ `defined | `started | `locked_in | `active | `failed ];
  bit: int option;
  start_time: int64;
  timeout: int64;
  since: int;
  min_activation_height: int option;
} [@@deriving encoding {ignore}]

type softfork = {
  typ: string; [@key "type"]
  active: bool;
  height: int option;
  bip9: bip9 option;
} [@@deriving encoding]

type info = {
  chain: string;
  blocks: int;
  headers: int;
  bestblockhash: string;
  difficulty: float;
  imediantime: int64; [@key "mediantime"]
  verificationprogress: float;
  initialblockdownload: bool;
  chainwork: string;
  size_on_disk: int64;
  pruned: bool;
  softforks: (string * softfork) list; [@assoc]
  warnings: string;
} [@@deriving encoding]

type header = {
  hash: string;
  confirmations: int;
  height: int;
  version: int64;
  versionHex: string;
  merkleroot: string;
  time: int64;
  mediantime: int64;
  nonce: int64;
  bits: string;
  difficulty: float;
  chainwork: string;
  nTx: int;
  previousblockhash: string; [@dft ""]
  nextblockhash: string option;
} [@@deriving encoding]

type 'tx block = {
  header: header; [@merge]
  strippedsize: int;
  size: int;
  weight: int;
  tx: 'tx list;
} [@@deriving encoding]

type none = unit [@encoding Json_encoding.(conv (fun () -> [||]) (fun _ -> ()) (array unit))]
[@@deriving encoding]

type filter = {
  f_filter: string;
  f_header: string;
} [@@deriving encoding]

type stats = {
  avgfee: int;
  avgfeerate: int;
  avgtxsize: int;
  blockhash: string;
  feerate_percentiles: (int * int * int * int * int);
  stat_height: int; [@key "height"]
  ins: int;
  maxfee: int;
  maxfeerate: int;
  maxtxsize: int;
  medianfee: int;
  stat_mediantime: int64; [@key "mediantime"]
  mediantxsize: int;
  minfee: int;
  minfeerate: int;
  mintxsize: int;
  outs: int;
  subsidy: int64;
  swtotal_size: int;
  swtotal_weight: int;
  swtxs: int;
  stat_time: int64; [@key "time"]
  total_out: int64;
  total_size: int;
  total_weight: int;
  totalfee: int;
  txs: int;
  utxo_increase: int;
  utxo_size_inc: int;
} [@@deriving encoding]

type hash_or_height = [`height of int | `hash of string]
[@@deriving encoding]

type tip = {
  t_height: int;
  t_hash: string;
  t_branchlen: int;
  t_status: [
    | `invalid | `headers_only [@key "headers-only"] | `valid_headers [@key "valid-headers"]
    | `valid_fork [@key "valid-fork"] | `active ]
} [@@deriving encoding]

type txstats = {
  txstat_time: int64; [@key "time"]
  txcount: int;
  window_final_block_hash: string;
  window_final_block_height: int;
  window_block_count: int;
  window_tx_count: int;
  window_interval: int;
  txrate: float;
} [@@deriving encoding]

type mempool_fee = {
  base: int;
  modified: int;
  ancestor: int;
  descendant: int;
} [@@deriving encoding]

type mempool_entry = {
  m_vsize: int;
  m_weight: int;
  m_fee: int;
  m_modifiedfee: int;
  m_time: int64;
  m_height: int;
  m_descendantcount: int;
  m_descendantsize: int;
  m_descendantfees: int;
  m_ancestorcount: int;
  m_ancestorsize: int;
  m_ancestorfees: int;
  m_wtxid: string;
  m_fees: mempool_fee;
  m_depends: string list;
  m_spentby: string list;
  m_replaceable: bool; [@key "bip125-replaceable"]
  m_unbroadcast: bool;
} [@@deriving encoding]

type mempool_info = {
  m_loaded: bool;
  m_size: int;
  m_bytes: int;
  m_usage: int;
  m_total_fee: float;
  m_maxmempool: int;
  m_mempoolminfee: float;
  m_minrelaytxfee: float;
  m_unbroadcastfee: int
} [@@deriving encoding]

type script_sig = {
  asm: string;
  hex: string;
} [@@deriving encoding {ignore}]

type txin_gen = {
  ori: string; [@key "txid"]
  index: int64; [@key "vout"]
  script_sig: script_sig; [@key "scriptSig"]
} [@@deriving encoding]

type txin_kind =
  | Gen of txin_gen
  | Coinbase of (Json_repr.ezjsonm [@wrap "coinbase"])
[@@deriving encoding]

type txin = {
  kind: txin_kind; [@merge]
  txinwitness: string list; [@dft []]
  sequence: int64;
} [@@deriving encoding]

type script_pubkey = {
  asm: string;
  hex: string;
  desc: string option;
  typ: string; [@key "type"]
  address: string option;
} [@@deriving encoding]

type txout = {
  value: float;
  n: int;
  pubkey: script_pubkey; [@key "scriptPubKey"]
} [@@deriving encoding]

type txout0 = {
  txo_bestblock: string;
  txo_confirmations: int;
  txo_value: float;
  txo_pubkey: script_pubkey; [@key "scriptPubKey"]
  txo_coinbase: bool;
} [@@deriving encoding]

type txn = {
  in_active_chain: bool option;
  hex: string;
  txid: string;
  txhash: string; [@key "hash"]
  txsize: int; [@key "size"]
  vsize: int;
  txweight: int; [@key "weight"]
  txversion: int64; [@key "version"]
  locktime: int64;
  vin: txin list;
  vout: txout list;
  blockhash: string option;
  txconfirmations: int option; [@key "confirmations"]
  blocktime: int64 option;
  txtime: int64 option; [@key "time"]
} [@@deriving encoding {ignore}]

type hash_type = [
  | `hash_serialized_2
  | `none
] [@@deriving encoding]

type txout_stat = {
  txos_height: int;
  txos_bestblock: string;
  txos_transactions: int64;
  txos_txouts: int64;
  txos_bogosize: int64;
  txos_hash_serialized_2: string option;
  txos_disk_size: int64;
  txos_total_amount: float;
} [@@deriving encoding]

type memory_info = {
  used: int64;
  free: int64;
  total: int64;
  locked: int64;
  chunks_used: int64;
  chunks_free: int64;
} [@@deriving encoding]

let memory_info_enc = obj1 (req "locked" memory_info_enc)

type active_command = {
  ac_method: string;
  ac_duration: float;
} [@@deriving encoding]

type rpc_info = {
  active_commands: active_command list;
  logpath: string
} [@@deriving encoding]

type _ meth =
  | Best : string meth
  | Block : string -> string block meth
  | Block0 : string -> string meth
  | Block2 : string -> txn block meth
  | Info : info meth
  | Count : int meth
  | Filter : string * string option -> filter meth
  | Blockhash : int -> string meth
  | Header : string -> header meth
  | Header0 : string -> string meth
  | Stats : hash_or_height -> stats meth
  | Tips : tip list meth
  | TxStats : int option * string option -> txstats meth
  | Difficulty : float meth
  (* | Mempool_ancestors *)
  (* | Mempool_descendants *)
  (* | Mempool_entry *)
  (* | Mempool_info : mempool_info meth *)
  (* | Raw_mempool0 : string list meth *)
  (* | Raw_mempool1 : (string * mempool_entry) list meth *)
  | Txout : string * int * bool option -> txout0 meth
  | Txout_proof : string list * string option -> string meth
  | Txout_setinfo : hash_type option -> txout_stat meth
  | Precious : string -> unit meth
  | Prune : int -> int meth
  | Save_mempool : unit meth
  (* | Scan_txoutset  *)
  | Verify_chain : int option * int option -> bool meth
  | Verify_txout : string -> string list meth

  | Memory_info_stats : memory_info meth
  | Memory_info_malloc : string meth
  | Rpc_info : rpc_info meth
  | Help : string meth
  (* | Logging *)
  | Stop : string meth
  | Uptime : int64 meth

  (* | Generate_block *)
  (* | Generate_to_address *)
  (* | Generate_to_descriptor *)

  (* | Block_template *)
  (* | Mining_info *)
  (* | Hashrate *)
  (* | Prioritise *)
  (* | Submit_block *)
  (* | Submit_header *)

  (* | Add_node *)
  (* | Clear_banned *)
  (* | Disconnect_node *)
  (* | Added_node_info *)
  (* | Connection_count *)
  (* | Net_totals *)
  (* | Network_info *)
  (* | Node_addresses *)
  (* | Peer_info *)
  (* | Banned *)
  | Ping : unit meth
  (* | Ban *)
  (* | Active_network *)

  (* | Analyze_psbt *)
  (* | Combine_pbst *)
  (* | Combine_raw_transaction *)
  (* | Convert_to_psbt *)
  (* | Create_psbt *)
  (* | Create_raw_transaction *)
  (* | Decode_psbt *)
  (* | Finalize_psbt *)
  (* | Fund_raw_transaction *)
  | Raw_transaction0 : string * string option -> string meth
  | Raw_transaction : string * string option -> txn meth
  (* | Join_psbts *)
  (* | Send_raw_transaction *)
  (* | Sign_raw_transaction *)
  (* | Mempool_accept *)
  (* | Update_psbt *)

  (* | Create_multisig *)
  (* | Derive_addresses *)
  (* | Estimate_fee *)
  (* | Descriptor_info *)
  (* | Index_info *)
  (* | Sign_message *)
  (* | Validate_address *)
  (* | Verify_message *)

  (* | Abandon_transaction *)
  (* | Abort_rescan *)
  (* | Add_multisig_address *)
  (* | Backup_wallet *)
  (* | Bump_fee *)
  (* | Create_wallet *)
  (* | Dump_privkey *)
  (* | Dump_wallet *)
  (* | Encrypt_wallet *)
  (* | Addresses_by_label *)
  (* | Address_info *)
  (* | Balance *)
  (* | Balances *)
  (* | New_address *)
  (* | Raw_change_address *)
  (* | Received_by_address *)
  (* | Received_by_label *)
  (* | Transaction *)
  (* | Unconfirmed_balance *)
  (* | Wallet_info *)
  (* | Import_address *)
  (* | Import_descriptors *)
  (* | Import_multi *)
  (* | Import_privkey *)
  (* | Import_pruned_funds *)
  (* | Import_pubkey *)
  (* | Import_wallet *)
  (* | Key_pool_refill *)
  (* | Address_groupings *)
  (* | Labels *)
  (* | Lock_unspent *)
  (* | ... *)
