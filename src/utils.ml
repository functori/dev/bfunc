open Types

module Let = struct
  let (let>) = Lwt.bind
  let (let|>) p f = Lwt.map f p
  let (let>?) p f = Lwt.bind p (function Error e -> Lwt.return_error e | Ok x -> f x)
  let (let|>?) p f = Lwt.map (Result.map f) p
  let rok = Lwt.return_ok
  let rerr = Lwt.return_error
end
open Let

let post_aux ?(id=`Null) ?auth ?(params=[]) base meth service arg =
  match Config.headers ?auth () with
  | None -> rerr {code=0; message="no user or password set"; data=None}
  | Some headers ->
    Fun.flip Lwt.map (EzReq_lwt.post0 ~headers ~input:{meth; params; id; arg} base service) @@ function
    | Error EzReq_lwt_S.UnknownError {code; msg} ->
      Error {code; message=Option.value ~default:"" msg; data=None}
    | Error EzReq_lwt_S.KnownError {code; error} ->
      Error {code; message=Printexc.to_string error; data=None}
    | Ok (Error e) -> Error e
    | Ok Ok x -> Ok x

let post (type o) ?id ?base (meth : o meth) : (o, error) result Lwt.t =
  match (base, Config.c.Config.node) with
  | None, None -> rerr {code=0; message="no node set"; data=None}
  | Some base, _ | _, Some base -> match meth with
    | Best -> post_aux ?id base "getbestblockhash" Services.best ()
    | Block hash -> post_aux ?id base "getblock" Services.block hash
    | Block0 hash -> post_aux ?id ~params:[`Float 0.] base "getblock" Services.block0 hash
    | Block2 hash -> post_aux ?id ~params:[`Float 2.] base "getblock" Services.block2 hash
    | Info -> post_aux ?id base "getblockchaininfo" Services.info ()
    | Count -> post_aux ?id base "getblockcount" Services.count ()
    | Blockhash height -> post_aux ?id base "getblockhash" Services.hash height
    | Header hash -> post_aux ?id base "getblockheader" Services.header hash
    | Header0 hash -> post_aux ?id ~params:[`Bool false] base "getblockheader" Services.header0 hash
    | Tips -> post_aux ?id base "getchaintips" Services.chaintips ()
    | Difficulty -> post_aux ?id base "getdifficulty" Services.difficulty ()
    | Raw_transaction0 (txid, bl) ->
      let params = `Bool false :: Option.fold ~none:[] ~some:(fun s -> [`String s]) bl in
      post_aux ?id ~params base "getrawtransaction" Services.rawtransaction txid
    | Raw_transaction (txid, bl) ->
      let params = `Bool true :: Option.fold ~none:[] ~some:(fun s -> [`String s]) bl in
      post_aux ?id ~params base "getrawtransaction" Services.transaction txid
    | Filter (bl, typ) ->
      let params = match typ with None -> [] | Some s -> [ `String s ] in
      post_aux ?id ~params base "getblockfilter" Services.filter bl
    | Stats blid -> post_aux ?id base "getblockstats" Services.blockstats blid
    | TxStats (nblock, hash) ->
      let params =
        (match nblock with None -> `Null | Some i -> `Float (float_of_int i)) ::
        (match hash with None -> [] | Some s -> [`String s]) in
      post_aux ?id ~params base "getchaintxstats" Services.txstats ()
    | Txout (txid, n, include_mempool) ->
      let params = Option.fold ~none:[] ~some:(fun b -> [ `Bool b ]) include_mempool in
      post_aux ?id ~params base "gettxout" Services.txout (txid, n)
    | Txout_proof (txids, bl) ->
      let params = Option.fold ~none:[] ~some:(fun s -> [ `String s ]) bl in
      post_aux ?id ~params base "gettxoutproof" Services.txout_proof txids
    | Txout_setinfo hash_type ->
      let params = Option.fold ~none:[] ~some:(fun s -> [ Json_encoding.construct hash_type_enc s ]) hash_type in
      post_aux ?id ~params base "gettxoutsetinfo" Services.txout_setinfo ()
    | Precious bl -> post_aux ?id base "preciousblock" Services.precious bl
    | Prune height -> post_aux ?id base "pruneblockchain" Services.prune height
    | Save_mempool -> post_aux ?id base "savemempool" Services.save_mempool ()
    | Verify_chain (lv, n) ->
      let params =
        (match lv with None -> `Float 3. | Some i -> `Float (float_of_int i)) ::
        (match n with None -> [] | Some i -> [`Float (float_of_int i)]) in
      post_aux ?id ~params base "verifychain" Services.verify_chain ()
    | Verify_txout p -> post_aux ?id base "verifyoutproof" Services.verify_txout p
    | Memory_info_stats -> post_aux ?id base "getmemoryinfo" Services.memory_info_stats ()
    | Memory_info_malloc ->
      post_aux ?id ~params:[`String "mallocinfo"] base "getmemoryinfo" Services.memory_info_malloc ()
    | Rpc_info -> post_aux ?id base "getrpcinfo" Services.rpc_info ()
    | Help -> post_aux ?id base "help" Services.help ()
    | Stop -> post_aux ?id base "stop" Services.stop ()
    | Uptime -> post_aux ?id base "uptime" Services.uptime ()
    | Ping -> post_aux ?id base "ping" Services.ping ()

let set_node s = Config.c.Config.node <- Some (EzAPI.BASE s)
let set_user s = Config.c.Config.user <- Some s
let set_password s = Config.c.Config.password <- Some s
