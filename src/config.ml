type t = {
  mutable node: EzAPI.base_url option;
  mutable user: string option;
  mutable password: string option;
}

let c = { node = None; user = None; password = None }

let headers ?auth () =
  let auth = match auth, c.user, c.password  with
    | Some x, _, _ -> Some x
    | _, Some u, Some p  -> Some (u, p)
    | _ -> None in
  match auth with
  | None -> None
  | Some (u, p) ->
    Some [ "Authorization", "Basic " ^ Base64.encode_exn (Format.sprintf "%s:%s" u p) ]
