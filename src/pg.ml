open Btc

[%%pg "bitcoin_indexer"]

let to_ s = Hex.to_string (`Hex s)
let of_ s = let `Hex s = Hex.of_string s in s

type block = {
  b_id: int64; [@serial] [@primary]
  b_hash: string;  [@type "bytea"] [@conv of_, to_]
  b_height: int32;
  b_time: int64;
  b_prev: int64;
  b_main: bool;
} [@@pg {name="blocks"; indexes=[b_hash]}]

type tx = {
  tx_id: int64; [@serial] [@primary]
  tx_txid: string; [@type "bytea"] [@conv of_, to_]
  tx_block: int64;
} [@@pg {name="txs"; indexes=[tx_txid; tx_block]}]

type txin = {
  txi_id: int64;
  txi_ori: int64;
  txi_vout: int32;
} [@@pg {name="txins"; indexes=[txi_id; txi_ori]}]

type txout = {
  txo_id: int64;
  txo_value: int64;
  txo_n: int32;
  txo_address: int64 option;
} [@@pg {name="txouts"; indexes=[txo_id]}]

type address = {
  a_id: int64; [@serial] [@primary]
  a_address: string;
} [@@pg {name="addresses"; indexes=[a_address]}]

let wrap0 dbh f = try f (); rok dbh with exn -> rerr @@ `pg exn
let wrap1 f = try rok @@ f () with exn -> rerr @@ `pg exn

let t00 = Unix.gettimeofday ()
let t0 = ref t00
let count_tx = ref 0
let count_txin = ref 0
let count_txout = ref 0
let count_address = ref 0

let set kind dbh b =
  wrap0 dbh @@ fun () ->
  match kind with
  | `unset -> [%pgsql dbh "update blocks set main=false where hash=${to_ b.header.hash}"]
  | `set -> [%pgsql dbh "update blocks set main=true where hash=${to_ b.header.hash}"]
  | `register ->
    let t1 = Unix.gettimeofday () in
    let dt = t1 -. !t0 in
    let dt0 = t1 -. t00 in
    t0 := t1;
    Format.printf "block %d %s %s %.2f | %d %d %d %d %.2f@." b.header.height b.header.hash CalendarLib.(Printer.Calendar.to_string @@ Calendar.from_unixfloat @@ Int64.to_float b.header.time) dt !count_tx !count_txin !count_txout !count_address dt0;
    let prev_id = match [%pgsql dbh "select id from blocks where hash = ${to_ b.header.previousblockhash}"] with
      | [ id ] -> id
      | _ -> -1L in
    PGOCaml.transact dbh @@ fun dbh ->
    match [%pgsql dbh
        "insert into blocks(hash, height, time, prev, main) \
         values(${to_ b.header.hash}, ${Int32.of_int b.header.height}, ${b.header.time}, \
         $prev_id, true) returning id"] with
    | [] -> ()
    | bl_id :: _ ->
      List.iter (fun (tx : txn) ->
        incr count_tx;
        match [%pgsql dbh "insert into txs(txid, block) values(${to_ tx.txid}, $bl_id) returning id"] with
        | [] -> ()
        | tx_id :: _ ->
          List.iter (fun txi -> match txi.kind with
            | Gen g ->
              incr count_txin;
              let ori = match [%pgsql dbh "select id from txs where txid = ${to_ g.ori} limit 1"] with
                | [] -> -1L
                | id :: _ -> id in
              [%pgsql dbh "insert into txins(id, ori, vout) values($tx_id, $ori, ${Int64.to_int32 g.index})"]
            | _ -> ()) tx.vin;
          List.iter (fun txo ->
            incr count_txout;
            let txo_value = Int64.of_float @@ Float.round (txo.value *. 100000000.) in
            let address = match txo.pubkey.address with
              | None -> None
              | Some address ->
                match [%pgsql dbh "select id from addresses where address = $address"] with
                | id :: _ -> Some id
                | [] -> match [%pgsql dbh "insert into addresses(address) values ($address) returning id"] with
                  | [] -> None
                  | id :: _ -> incr count_address; Some id in
            [%pgsql dbh "insert into txouts(id, value, n, address) values($tx_id, $txo_value, ${Int32.of_int txo.n}, $?address)"]
          ) tx.Btc.vout;
      ) b.tx

let mk_tx ?(vin=[]) ?(vout=[]) txid = {
  in_active_chain=None; hex=""; txid; txhash=""; txsize=0; vsize=0; txweight=0;
  txversion=0L; locktime=0L; vin; vout; blockhash=None; txconfirmations=None;
  blocktime=None; txtime=None
}

let mk ?(tx=[]) ?(prev="") b =
  let header = {
    hash=of_ b#hash; confirmations=0; height=Int32.to_int b#height; version=0L; versionHex=""; merkleroot="";
    time=b#time; mediantime=0L; nonce=0L; bits=""; difficulty=0.; chainwork="";
    nTx=List.length tx; previousblockhash=prev; nextblockhash=None } in
  {header; tx; size=0; weight=0; strippedsize=0}

let get dbh hash =
  wrap1 @@ fun () ->
  match [%pgsql.object dbh "select * from blocks where hash = ${to_ hash} limit 1"] with
  | [] -> `unknown
  | b :: _ ->
    let tx = List.map (fun s -> mk_tx (of_ s)) [%pgsql dbh "select txid from txs where block = ${b#id}"] in
    let prev = match [%pgsql dbh "select hash from blocks where id = ${b#prev}"] with
      | [] -> ""
      | hash :: _ -> of_ hash in
    let bl = mk ~tx ~prev b in
    if b#main then `main bl else `alt bl

let head dbh =
  wrap1 @@ fun () ->
  match [%pgsql.object dbh "select * from blocks order by height desc limit 1"] with
  | [] -> None
  | b :: _ ->
    let prev = match [%pgsql dbh "select hash from blocks where id = ${b#prev}"] with
      | [] -> ""
      | hash :: _ -> of_ hash in
    Some (mk ~prev b)

let db () = { Crawl.set; get; head; acc=PGOCaml.connect () }
