open Btc

type config = {
  start: int; [@req]
  node: string; [@dft "http://localhost:18332"]
  user: string; [@dft "user"]
  password: string [@dft "password"]
} [@@deriving arg {exe="test_crawl.exe"}]

let print_error = function
  | `http (code, reason) -> Format.eprintf "Http error %d: %s@." code reason
  | `wrong_state -> Format.eprintf "Wrong state@."
  | `stopped -> Format.eprintf "Stopped@."
  | `btc e -> Format.eprintf "Btc error %d: %s@." e.code e.message
  | `exn exn -> Format.eprintf "Exn: %s@." (Printexc.to_string exn)
  | `pg exn -> Format.eprintf "Pg exn: %s@." (Printexc.to_string exn)

let () =
  let c = parse_config () in
  Utils.set_user c.user;
  Utils.set_password c.password;
  Utils.set_node c.node;
  let a = { Crawl.db = Pg.db (); u = Chain.util; chain = Chain.chain; stop } in
  Lwt_main.run @@ Lwt.map (Result.iter_error print_error) @@ Crawl.main ~start:c.start a
