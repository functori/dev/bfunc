open Btc

let arg i = try Some (Array.get Sys.argv i) with _ -> None
let wrapr r f = match r with
  | Error {code; message; data} ->
    Format.printf "error %d: %s%s@." code message
      (match data with None -> "" | Some d -> "\n0x" ^ (d :> string))
  | Ok x -> f x
let print enc x =
  Format.printf "ok\n%s@." @@ EzEncoding.construct ~compact:false enc x

let dft_block = "000000000000001315898c84d2d9e37fef38db8025265c0047a2c92552459936"
let dft_txid = "362d1669bebf48dcf71abb29f3c6e311bf5cd11bf3dee2d8c422ee9ebb8f0234"

let () =
  Utils.set_user "user";
  Utils.set_password "password";
  Utils.set_node "http://localhost:18332"

let best () =
  let|> r = post Best in
  wrapr r @@ fun s ->
  Format.printf "ok: %s@." s

let block () =
  let h = Option.value ~default:dft_block @@ arg 2 in
  let v = Option.fold ~none:1 ~some:int_of_string @@ arg 3 in
  match v with
  | 0 ->
    let|> r = post (Block0 h) in
    wrapr r @@ fun s ->
    Format.printf "ok:\n%s@." s
  | 1 ->
    let|> r = post (Block h) in
    wrapr r @@ fun b ->
    print (block_enc Json_encoding.string) b
  | _ ->
    let|> r = post (Block2 h) in
    wrapr r @@ fun b ->
    print (block_enc txn_enc) b

let header () =
  let h = Option.value ~default:dft_block @@ arg 2 in
  let v = Option.fold ~none:true ~some:bool_of_string @@ arg 3 in
  if v then
    let|> r = post (Header h) in
    wrapr r @@ fun h ->
    print header_enc h
  else
    let|> r = post (Header0 h) in
    wrapr r @@ fun s ->
    Format.printf "ok:\n%s@." s

let count () =
  let|> r = post Count in
  wrapr r @@ fun i ->
  Format.printf "ok: %d@." i

let blockhash () =
  let i = Option.fold ~none:2420273 ~some:int_of_string @@ arg 2 in
  let|> r = post (Blockhash i) in
  wrapr r @@ fun s ->
  Format.printf "ok: %s@." s

let info () =
  let|> r = post Info in
  wrapr r @@ fun i ->
  print info_enc i

let difficulty () =
  let|> r = post Difficulty in
  wrapr r @@ fun d ->
  print Json_encoding.float d

let chaintips () =
  let|> r = post Tips in
  wrapr r @@ fun ct ->
  print (Json_encoding.list tip_enc) ct

let transaction () =
  let txid = Option.value ~default:dft_txid @@ arg 2 in
  let bl = arg 3 in
  let|> r = post @@ Raw_transaction (txid, bl) in
  wrapr r @@ fun txn ->
  print txn_enc txn

let filter () =
  let bl = Option.value ~default:dft_block @@ arg 2 in
  let typ = arg 3 in
  let|> r = post @@ Filter (bl, typ) in
  wrapr r @@ fun f ->
  print filter_enc f

let stats () =
  let bl = match arg 2 with
    | None -> `hash dft_block
    | Some x -> match int_of_string_opt x with
      | Some i -> `height i
      | None -> `hash x in
  let|> r = post @@ Stats bl in
  wrapr r @@ fun f ->
  print stats_enc f

let txstats () =
  let n = Option.map int_of_string @@ arg 2 in
  let bl = arg 3 in
  let|> r = post @@ TxStats (n, bl) in
  wrapr r @@ fun s ->
  print txstats_enc s

let txout () =
  let txid = Option.value ~default:dft_txid @@ arg 2 in
  let n = Option.fold ~none:0 ~some:int_of_string @@ arg 3 in
  let|> r = post @@ Txout (txid, n, None) in
  wrapr r @@ fun s ->
  print txout0_enc s

let txout_proof () =
  let txids = Option.fold ~none:[dft_txid]
      ~some:(String.split_on_char ',') @@ arg 2 in
  let bl = arg 3 in
  let|> r = post @@ Txout_proof (txids, bl) in
  wrapr r @@ fun s ->
  Format.printf "ok: %s@." s

let txout_setinfo () =
  let|> r = post @@ Txout_setinfo None in
  wrapr r @@ fun s ->
  print txout_stat_enc s

let save_mempool () =
  let|> r = post @@ Save_mempool in
  wrapr r @@ fun () ->
  Format.printf "ok@."

let verify_chain () =
  let lv = Option.map int_of_string @@ arg 2 in
  let n = Option.map int_of_string @@ arg 3 in
  let|> r = post @@ Verify_chain (lv, n) in
  wrapr r @@ fun b ->
  Format.printf "ok %B@." b

let memory_info () =
  match arg 2 with
  | Some "malloc" ->
    let|> r = post @@ Memory_info_malloc in
    wrapr r @@ fun s ->
    Format.printf "ok %s@." s
  | _ ->
    let|> r = post @@ Memory_info_stats in
    wrapr r @@ fun s ->
    print memory_info_enc s

let rpc_info () =
  let|> r = post Rpc_info in
  wrapr r @@ fun s ->
  print rpc_info_enc s

let help () =
  let|> r = post Help in
  wrapr r @@ fun s ->
  Format.printf "ok\n%s@." s

let stop () =
  let|> r = post Stop in
  wrapr r @@ fun s ->
  Format.printf "ok %s@." s

let uptime () =
  let|> r = post Uptime in
  wrapr r @@ fun i ->
  Format.printf "ok %Ld@." i

let ping () =
  let|> r = post Ping in
  wrapr r @@ fun () ->
  Format.printf "ok@."

let () =
  Lwt_main.run @@
  match arg 1 with
  | Some "best" -> best ()
  | Some "block" -> block ()
  | Some "blockhash" -> blockhash ()
  | Some "info" -> info ()
  | Some "header" -> header ()
  | Some "difficulty" -> difficulty ()
  | Some "chaintips" -> chaintips ()
  | Some "transaction" -> transaction ()
  | Some "filter" -> filter ()
  | Some "stats" -> stats ()
  | Some "count" -> count ()
  | Some "txstats" -> txstats ()
  | Some "txout" -> txout ()
  | Some "txoutproof" -> txout_proof ()
  | Some "txoutsetinfo" -> txout_setinfo ()
  | Some "save_mempool" -> save_mempool ()
  | Some "verify_chain" -> verify_chain ()
  | Some "memory_info" -> memory_info ()
  | Some "rpc_info" -> rpc_info ()
  | Some "help" -> help ()
  | Some "stop" -> stop ()
  | Some "uptime" -> uptime ()
  | Some "ping" -> ping ()
  | _ -> Format.printf "unknown test@."; exit 1
