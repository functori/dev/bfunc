open Btc

let head = ref None
let mem : (string, header * bool) Hashtbl.t = Hashtbl.create 1024

let set kind () b =
  let h = b.header in
  let kind, main = match kind with
    | `unset -> "unset", false
    | `register -> "register", true
    | `set -> "set", true in
  Format.printf "%s %s (%d)@." kind h.hash h.height;
  Hashtbl.add mem h.hash (h, main);
  if main then (match !head with
    | None -> head := Some h
    | Some he when he.height < h.height -> head := Some h
    | _ -> ());
  Lwt.return_ok ()

let mk header = { header; strippedsize=0; size=0; weight=0; tx=[] }

let get () s =
  Format.printf "get %s@." s;
  match Hashtbl.find_opt mem s with
  | None -> Lwt.return_ok `unknown
  | Some (h, true) -> Lwt.return_ok (`main (mk h))
  | Some (h, false) -> Lwt.return_ok (`alt (mk h))

let head () =
  Format.printf "head@.";
  Lwt.return_ok (Option.map mk !head)

let print_error = function
  | `http (code, reason) -> Format.eprintf "Http error %d: %s@." code reason
  | `wrong_state -> Format.eprintf "Wrong state@."
  | `stopped -> Format.eprintf "Stopped@."
  | `btc e -> Format.eprintf "Btc error %d: %s@." e.code e.message
  | `exn exn -> Format.eprintf "Exn %s@." (Printexc.to_string exn)

let stopr = ref false
let stop () = Lwt.return !stopr

type config = {
  start: int; [@req]
  node: string; [@dft "http://localhost:18332"]
  user: string; [@dft "user"]
  password: string [@dft "password"]
} [@@deriving arg {exe="test_crawl.exe"}]

let () =
  let c = parse_config () in
  Utils.set_user c.user;
  Utils.set_password c.password;
  Utils.set_node c.node;
  let a = { Crawl.db = {Crawl.set; get; head; acc=()}; u = Chain.util; chain = Chain.chain; stop } in
  Lwt_main.run @@ Lwt.map (Result.iter_error print_error) @@ Crawl.main ~start:c.start a
